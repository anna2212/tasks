#include <string.h>

int CharToInt(const char *str)
{
    int result = 0;
    while(*str!='\0')
    {
        result*=10;
        result+=*str-'0';
        ++str;
    }
 
    return result;
} 

int parse_time(const char* str) 
{
    int hours = 0;
    int size = strlen (str);
    char number_s[size] = {'\0'};
    int j=0;
    for (int i=0; i < size; i++)
    {
        if (str[i] >= '0' && str[i] <= '9')
        {
            number_s[j]=str[i];
            j++;
        }
        else
        {
            switch (str[i])
            {
                case 'h':
                    hours = hours + CharToInt(number_s);
                    break;

                case 'd':
                    hours = hours + CharToInt(number_s)*8;
                    break;

                case 'w':
                    hours = hours + CharToInt(number_s)*40;
                    break;

                case ' ':
                    break;

                default:
                return -1; //ошибка
            }
            
            if (str[i] != ' ')
            {
                for (int i=0; i<size; i++)
                    number_s[i]='\0';
                j=0;
            }
        }    
    }
    return hours;
}