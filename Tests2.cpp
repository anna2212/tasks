#include "Task2.cpp"
#include <gtest/gtest.h>

TEST(Expression_Test, Parse_Expression) { 
    ASSERT_EQ(9, parse_expression("9"));
    ASSERT_EQ(10, parse_expression("9+1"));
    ASSERT_EQ(8, parse_expression("9-1"));
    ASSERT_EQ(12, parse_expression("3*4"));
    ASSERT_EQ(11, parse_expression("55/5"));
    ASSERT_EQ(6, parse_expression("1+2+3"));
    ASSERT_EQ(9, parse_expression("(1+2)*3"));
    ASSERT_EQ(6, parse_expression("(1+2)*4/2)"));
    ASSERT_EQ(0, parse_expression("n"));
    ASSERT_EQ(0, parse_expression("*"));
}
 
int main(int argc, char **argv) 
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}