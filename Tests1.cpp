#include "Task1.cpp"
#include <gtest/gtest.h>
 
TEST (JiraTest, IntoInt)
{
    ASSERT_EQ(1, CharToInt("1"));
    ASSERT_EQ(123, CharToInt("123"));
}

TEST(JiraTest, IntoHours) 
{ 
    ASSERT_EQ(1, parse_time("1h"));
    ASSERT_EQ(8, parse_time("1d"));
    ASSERT_EQ(40, parse_time("1w"));
    ASSERT_EQ(123, CharToInt("123"));
    ASSERT_EQ(12, parse_time("12h"));
    ASSERT_EQ(9, parse_time("1d 1h"));
    ASSERT_EQ(59, parse_time("1w 2d 3h"));
    ASSERT_EQ(-1, parse_time("n"));
    ASSERT_EQ(0, parse_time("d"));
    ASSERT_EQ(-1, parse_time(" k"));
    ASSERT_EQ(0, parse_time(" 8"));
}
 
 
int main(int argc, char **argv) 
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}